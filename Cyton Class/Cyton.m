%%Ashley Baipas
%%SN: 99201372
%%Assignment 2
%%UTS Robotics
classdef Cyton < handle
    properties
        self;
        model;
        StartPosture = [pi/2 -pi/4 -pi/2 -pi/2 pi/2 0];
        maxReachHeight = [0 -pi/2 0 -pi/2 0 0];
        maxReachSide = [0 0 0 -pi/2 0 0];
        q0 = [0 0 0 0 0 0 0];
        workspace = [-1 1 -1 1 -0.1 1];      
    end
    
    methods%% Class for Cyton robot simulation
function self = Cyton(name)
    if nargin > 0
        self.GetCytonRobot(name);
    end
    self.GetCytonRobot("test");
%> Define the boundaries of the workspace
end

%% GetUR5Robot
% Given a name (optional), create and return a UR5 robot model
function GetCytonRobot(self, name)
%     if nargin < 1
        % Create a unique name (ms timestamp after 1ms pause)
        pause(0.001);
        name = ['Cyton',datestr(now,'yyyymmddTHHMMSSFFF')];
%     end

    L1 = Link('d',0.10335,'a',0,'alpha', pi/2, 'qlim', [deg2rad(-360), deg2rad(360)], 'revolute', 'offset', 0);
    L2 = Link('d', 0, 'a', 0,'alpha',-pi/2, 'qlim', [deg2rad(-360), deg2rad(360)], 'revolute', 'offset', 0);
    L3 = Link('d',0.1,'a',0,'alpha', pi/2, 'qlim', [deg2rad(-360), deg2rad(360)], 'revolute', 'offset', pi/2);
    L4 = Link('d',0,'a',0.1,'alpha',-pi/2,'qlim',[deg2rad(-360),deg2rad(360)], 'revolute', 'offset', 0);
    L5 = Link('d',0,'a',0.1,'alpha',pi/2,'qlim',[deg2rad(-360),deg2rad(360)], 'revolute', 'offset', 0);
    L6 = Link('d',0,'a',0,'alpha',pi/2,'qlim',[deg2rad(-360),deg2rad(360)], 'revolute', 'offset', pi/2);
    L7 = Link('d',0.1,'a',0,'alpha',0,'qlim',[deg2rad(-360),deg2rad(360)], 'revolute', 'offset', 0);

    self.model = SerialLink([L1 L2 L3 L4 L5 L6 L7],'name',name);
end
%% PlotAndColourRobot
% Given a robot index, add the glyphs (vertices and faces) and
% colour them in if data is available 
function PlotAndColourRobot(self)%robot,workspace)
    for linkIndex = 0:self.model.n
            [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['CytonLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
        self.model.faces{linkIndex+1} = faceData;
        self.model.points{linkIndex+1} = vertexData;
    end

    % Display robot
    self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);
    if isempty(findobj(get(gca,'Children'),'Type','Light'))
        camlight
    end  
    self.model.delay = 0;

    % Try to correctly colour the arm (if colours are in ply file data)
    for linkIndex = 0:self.model.n
        handles = findobj('Tag', self.model.name);
        h = get(handles,'UserData');
        try 
            h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                          , plyData{linkIndex+1}.vertex.green ...
                                                          , plyData{linkIndex+1}.vertex.blue]/255;
            h.link(linkIndex+1).Children.FaceColor = 'interp';
        catch ME_1
            disp(ME_1);
            continue;
        end
    end
end     
%%
function RobotBase(self,transform)
    
    self.model.base = transform;

end
%%

function PlotRobot3d(self)
    for linkIndex = 0:self.model.n
            [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['CytonLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
        self.model.faces{linkIndex+1} = faceData;
        self.model.points{linkIndex+1} = vertexData;
    end

    % Display robot
    self.model.plot3d(self.StartPosture,'noarrow','workspace',self.workspace, 'view', [180 45]);
    if isempty(findobj(get(gca,'Children'),'Type','Light'))
        camlight
    end  
    self.model.delay = 0;

    % Try to correctly colour the arm (if colours are in ply file data)
    for linkIndex = 0:self.model.n
        handles = findobj('Tag', self.model.name);
        h = get(handles,'UserData');
        try 
            h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                          , plyData{linkIndex+1}.vertex.green ...
                                                          , plyData{linkIndex+1}.vertex.blue]/255;
            h.link(linkIndex+1).Children.FaceColor = 'interp';
        catch ME_1
            disp(ME_1);
            continue;
        end
    end
       
    
    
end

function PlotSimpleRobot(self)
    scale = 0.5;
    self.model.plot(self.q0, 'workspace', self.workspace,'scale',scale);
    self.model.teach();
end

function PlaySimpleRobot(self, matrix)
   scale = 0.5;
   self.model.plot(matrix, 'workspace', self.workspace,'scale', scale);
    
end

function qMatrix = calculateTraj(self, q1, q2)
    
    steps = 50;
    qMatrix = jtraj(q1,q2,steps);
    
end

function playRobot3d(self, matrix)
   
    animate(self.model, matrix);
    
end



function MaxHeight(self)
   
    steps = 50;
    qMatrix = jtraj(self.StartPosture,self.maxReachHeight,steps);
    
    for i = 1:50
    animate(self.model,qMatrix(i,1:6));drawnow;
    end
    
    self.model.fkine(self.maxReachHeight)
    
    qMatrix = jtraj(self.maxReachHeight,self.StartPosture,steps);
    
    for i = 1:50
    animate(self.model,qMatrix(i,1:6));drawnow;
    end
    
end

function MaxSide(self)
    steps = 50;
    qMatrix = jtraj(self.StartPosture,self.maxReachSide,steps);
    
    for i = 1:50
    animate(self.model,qMatrix(i,1:6));drawnow;
    end
    
    self.model.fkine(self.maxReachSide)
    
    qMatrix = jtraj(self.maxReachSide,self.StartPosture,steps);
    
    for i = 1:50
    animate(self.model,qMatrix(i,1:6));drawnow;
    end
    
end

function fkine = ForwardKinetics(self, q)
   fkine = self.model.fkine(q);
end

function ikcon = InverseKinetics(self, transform, q)
   ikcon = self.model.ikcon(transform, q);  
end

function pointCloudHeck(self)
stepRads = deg2rad(60);
qlim = self.model.qlim;
pointCloudeSize = prod(floor((qlim(1:5,2)-qlim(1:5,1))/stepRads + 1));
pointCloud = zeros(pointCloudeSize,3);

counter = 1;
tic

for q1 = qlim(1,1):stepRads:qlim(1,2)
    for q2 = qlim(2,1):stepRads:qlim(2,2)
        for q3 = qlim(3,1):stepRads:qlim(3,2)
            for q4 = qlim(4,1):stepRads:qlim(4,2)
                for q5 = qlim(5,1):stepRads:qlim(5,2)
                    % Don't need to worry about joint 6, just assume it=0
                    q6 = 0;
%                     for q6 = qlim(6,1):stepRads:qlim(6,2)
                        q = [q1,q2,q3,q4,q5,q6];
                        tr = self.model.fkine(q);                        
                        pointCloud(counter,:) = tr(1:3,4)';
                        counter = counter + 1; 
                        if mod(counter/pointCloudeSize * 100,1) == 0
                            display(['After ',num2str(toc),' seconds, completed ',num2str(counter/pointCloudeSize * 100),'% of poses']);
                        end
%                     end
                end
            end
        end
    end
end

 plot3(pointCloud(:,1),pointCloud(:,2),pointCloud(:,3),'r.');
 matrix = self.model.fkine(self.maxReachSide); 
 volume = ((self.model.base(1,4)- matrix(1,4))^3*(4/3))*pi;
 display(['Volume is: ' num2str(volume) 'm^3']);
end

    end


end